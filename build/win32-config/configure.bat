@echo off

net session >nul 2>&1
if not "%errorLevel%" == "0" (
    echo Configuration must be done with administrator permissions.
    echo Press any key to continue...
    pause >nul
    exit
)

set /p PROJECT_HOME=Enter project home directory (contains folders "src", "build", "include", "lib"...): 
set /p OPENCV_HOME=Enter OpenCV install directory (contains folders "x64" and "include"): 

echo ^> Making symbolic links...
mkdir "%PROJECT_HOME%\include"
mkdir "%PROJECT_HOME%\lib"
mklink /J "%PROJECT_HOME%\include\opencv" "%OPENCV_HOME%\include\opencv"
mklink /J "%PROJECT_HOME%\include\opencv2" "%OPENCV_HOME%\include\opencv2"
mklink /J "%PROJECT_HOME%\lib64\opencv" "%OPENCV_HOME%\x64\mingw\lib"

echo ^> Copying DLLs...
for /F "delims=" %%i in ('dir /B "%OPENCV_HOME%\x64\mingw\bin\*.dll"') do copy "%OPENCV_HOME%\x64\mingw\bin\%%i" "%PROJECT_HOME%\release"

echo Configuration done!
echo Press any key to continue...
pause >nul
