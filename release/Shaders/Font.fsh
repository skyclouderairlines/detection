#version 110
#define ANTIALIASING_SUBPIXEL

uniform sampler2D Texture;
uniform float GrayFactor;
uniform float SmoothFactor;
uniform float TextureScreenSize;
uniform vec3 BackColor;

void main() {
#ifdef ANTIALIASING_SUBPIXEL
	float col[3], shift = 1.0 / TextureScreenSize / 3.0 * 1.0, alpha = 0.0;
	for (int i = 0; i < 3; i++) {
		col[i] = texture2D(Texture, vec2(gl_TexCoord[0].x + (float(i) - 1.0) * shift, gl_TexCoord[0].y)).a - 0.5;
		col[i] = clamp(col[i] / GrayFactor / SmoothFactor + 0.5, 0.0, 1.0);
		if (col[i] > 0.0) alpha = 1.0/*max(alpha, pow(col[i], 0.4))*/;
	}
	gl_FragColor = vec4(mix(BackColor, gl_Color.rgb, vec3(col[0], col[1], col[2])), alpha);
#else
	float col = texture2D(Texture, gl_TexCoord[0].xy).a - 0.5;
	col = clamp(col / GrayFactor / SmoothFactor + 0.5, 0.0, 1.0);
	gl_FragColor = vec4(gl_Color.rgb, col * gl_Color.a);
#endif
}
