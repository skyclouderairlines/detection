#include <stdlib.h>
#include <algorithm>
#include "config.h"
#include "window.h"
#include "renderer.h"
#include "vertexarray.h"
#include "camera.h"
#include "framebuffer.h"
#include "bitmap.h"
#include "textrenderer.h"
#include "gui.h"
#include "test.h"

#ifdef PROJECTNAME_TARGET_WINDOWS
#include <Windows.h>
#include <SDL2/SDL_syswm.h>
#else
#include <gtk/gtk.h>
#endif

const double Pi = 3.1415926535898;

inline int log2Ceil(int x) {
	int res = 0;
	x--;
	while (x) res++, x /= 2;
	return res;
}

// GUI

std::string filename;

#ifdef PROJECTNAME_TARGET_WINDOWS

bool selectFile(const Window& win) {
	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version);
	SDL_GetWindowWMInfo(win.handle(), &wmInfo);
	HWND hwnd = wmInfo.info.win.window;
	
	OPENFILENAME ofn;
	char szFileName[MAX_PATH];
	ZeroMemory(&ofn, sizeof(ofn));
	szFileName[0] = 0;

	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = "Bitmap (*.bmp)\0*.bmp\0All Files (*.*)\0*.*\0\0";
	ofn.lpstrFile = szFileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrDefExt = "bmp";
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST;

	if (GetOpenFileName(&ofn)) {
		filename = szFileName;
		return true;
	}
	return false;
}

#else

bool selectFile(const Window& win) {
	bool res = false;
	GtkWidget* dialog = gtk_file_chooser_dialog_new(
		"Open File", 0, GTK_FILE_CHOOSER_ACTION_OPEN,
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, 0
	);
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		char* pFileName = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		if (pFileName) {
			filename = std::string(pFileName);
			g_free(pFileName);
			res = true;
		}
	}
	gtk_widget_destroy(dialog);
	while (gtk_events_pending()) gtk_main_iteration();
	return res;
}

#endif

std::vector<Texture> img;
std::vector<GUI::PictureBox> imgPictureBoxes;
std::vector<GUI::Label> captionLabels;

void updateGUI(GUI::ScrollArea& resultArea) {
	using GUI::Position;
	using GUI::Point2D;
	
	resultArea.children().clear();
	img.clear();
	imgPictureBoxes.clear();
	captionLabels.clear();
	
	int w = 20, h = 0;
	for (int i = 0; i < Test::numPasses; i++) {
		int cw = Test::pass[i].width(), ch = Test::pass[i].height();
		h = std::max(h, ch + 60);
		w += 20 + cw;
	}
	resultArea.size = Point2D(w, h);
	
	int curr = 0;
	for (int i = 0; i < Test::numPasses; i++) {
		int cw = Test::pass[i].width(), ch = Test::pass[i].height();
		int size = 1 << log2Ceil(std::max(cw, ch));
		img.push_back(Test::pass[i].resample(size, size));
		curr += 20;
		imgPictureBoxes.emplace_back(Position(float(curr) / w, 40.0f / h, 0, 0), Position(float(curr + cw) / w, (40.0f + ch) / h, 0, 0), nullptr);
		captionLabels.emplace_back(Position(float(curr) / w, 20.0f / h, 0, 0), Position(float(curr + cw) / w, 30.0f / h, 0, 0), Test::passLabel[i]);
		curr += cw;
	}
	
	for (int i = 0; i < Test::numPasses; i++) {
		imgPictureBoxes[i].picture = &img[i];
		resultArea.addChild({&imgPictureBoxes[i], &captionLabels[i]});
	}
}

// Parameters

struct Parameter {
	std::string name;
	int* ip, imn, imx;
	double* p, mn, mx;
	bool integral;
	Parameter(const char* name_, double* p_, double mn_, double mx_, double def): name(name_), p(p_), mn(mn_), mx(mx_), integral(false) { *p = def; }
	Parameter(const char* name_, int* ip_, int imn_, int imx_, int idef): name(name_), ip(ip_), imn(imn_), imx(imx_), integral(true) { *ip = idef; }
};

namespace Params {
	int count = 0;
	bool modified;
	GUI::Area controlArea(GUI::Position(0.0f, 0.0f, 0, 0), GUI::Position(1.0f, 1.0f, 0, 0));
	std::vector<Parameter> pars;
	std::vector<GUI::TrackBar> parTrackBars;
	
	void add(const Parameter& par) {
		using GUI::Position;
		pars.push_back(par);
		if (par.integral) parTrackBars.emplace_back(Position(0.0f, 0.0f, +10, count * 40 + 10), Position(1.0f, 0.0f, -10, count * 40 + 40), par.imn, par.imx, *par.ip, "");
		else parTrackBars.emplace_back(Position(0.0f, 0.0f, +10, count * 40 + 10), Position(1.0f, 0.0f, -10, count * 40 + 40), par.mn, par.mx, *par.p, "");
		count++;
	}
	
	void init() {
		for (GUI::TrackBar& c: parTrackBars) controlArea.addChild(&c);
		modified = true;
	}

	void update() {
		for (int i = 0; i < count; i++) {
			if (parTrackBars[i].modified()) modified = true;
			std::stringstream ss;
			if (pars[i].integral) {
				*pars[i].ip = std::floor(parTrackBars[i].value + 0.5);
				parTrackBars[i].value = *pars[i].ip;
				ss << pars[i].name << ": " << *pars[i].ip;
				parTrackBars[i].text = ss.str();
			} else {
				*pars[i].p = parTrackBars[i].value;
				ss << pars[i].name << ": " << *pars[i].p;
				parTrackBars[i].text = ss.str();
			}
		}
	}
}

// Main

int main(int argc, char** argv){
	// Initialize
	Config::load();
	Window::init();
#ifndef PROJECTNAME_TARGET_WINDOWS
	gtk_init(&argc, &argv);
#endif
	float scaling = Window::getScalingFactor();
	std::stringstream ss;
	ss << "Scaling factor = " << scaling;
	GUI::setScalingFactor(scaling);
	LogInfo(ss.str());
	Window& win = Window::getDefaultWindow("gra_cpp", 852 * scaling, 480 * scaling);
	Renderer::init();
	TextRenderer::loadAscii(std::string(FontPath) + "Ascii"); // TEMP CODE
	
	// Create GUI
	Renderer::setClearColor(Vec3f(0.15f));
	using GUI::Position;
	using GUI::Point2D;
	
	GUI::Area formArea(Position(0.0f, 0.0f, 0, 0), Position(1.0f, 1.0f, 0, 0));
	GUI::Area topArea(Position(0.0f, 0.0f, 0, 0), Position(0.7f, 0.0f, 0, +45));
	GUI::ScrollArea leftArea(Position(0.0f, 0.0f, 0, +45), Position(0.7f, 1.0f, 0, 0), Point2D(0, 0), Point2D(0, 0));
	GUI::Area rightArea(Position(0.7f, 0.0f, 0, 0), Position(1.0f, 1.0f, 0, 0));
	GUI::Button openButton(Position(0.0f, 0.0f, +10, +10), Position(0.0f, 1.0f, +130, -10), "Open File...");
	GUI::Label tipLabel(Position(0.0f, 0.0f, +140, +10), Position(1.0f, 1.0f, -10, -10), "Tip: press -/= to scale");
	GUI::Button okButton(Position(0.0f, 1.0f, +10, -40), Position(1.0f, 1.0f, -10, -10), "OK");

	formArea.addChild({&topArea, &leftArea, &rightArea});
	topArea.addChild({&tipLabel, &openButton});
	rightArea.addChild({&Params::controlArea, &okButton});
	GUI::Form form;
	form.area = &formArea;
	
//	Params::add(Parameter("Threshold", &Test::threshold, 0, 100, 12));
//	Params::add(Parameter("Largest N", &Test::sort_num, 0, 20, 10));
//	Params::add(Parameter("Entropy Threshold", &Test::entropythres, 0.0, 1.0, 0.2));
	Params::add(Parameter("Low Threshold", &Test::a, 0, 1000, 300));
	Params::add(Parameter("High - Low", &Test::b, 0, 1000, 500));
	Params::add(Parameter("Edge Thickness", &Test::expansion, 0, 20, 5));
//	Params::add(Parameter("Aperture Size", &Test::aperture, 3, 7, 3));
//	Params::add(Parameter("Radius", &Test::radius, 1, 40, 20));
//	Params::add(Parameter("Sample Count", &Test::sampleCount, 1, 400, 100));
//	Params::add(Parameter("Foreground Frac.", &Test::fgFraction, 0.0, 1.0, 0.02));
	Params::add(Parameter("R", &Test::color.x, 0, 255, 28));
	Params::add(Parameter("G", &Test::color.y, 0, 255, 67));
	Params::add(Parameter("B", &Test::color.z, 0, 255, 66));
	Params::add(Parameter("Distance", &Test::dist, 0, 100, 24));
	Params::add(Parameter("Erosion", &Test::erode, 0, 20, 3));
	Params::init();

	// Main Loop
	
	bool fileChanged = false;
	
	while (!win.shouldQuit()) {
		Renderer::setRenderArea(0, 0, win.getWidth(), win.getHeight());
		Renderer::clear();
		Renderer::setProjection(Mat4f::ortho(0, win.getWidth(), 0, win.getHeight(), -1, 1));
		Renderer::setModelview(Mat4f(1.0f));

		// Draw GUI
		Renderer::enableCullFace();
		Renderer::enableDepthTest();
		Renderer::enableBlend();
		Renderer::enableStencilTest();
		Renderer::disableTexture2D();
		Renderer::disableAlphaTest();
		form.render(win, Point2D(0, 0), Point2D(win.getWidth(), win.getHeight()));
		Renderer::waitForComplete();
		Renderer::checkError();
		win.swapBuffers();

		win.pollEvents();
		if (win.isKeyActed(SDL_SCANCODE_EQUALS)) leftArea.scale *= 1.2f;
		if (win.isKeyActed(SDL_SCANCODE_MINUS)) leftArea.scale /= 1.2f;

		// Update GUI
		form.update(win, Point2D(0, 0), Point2D(win.getWidth(), win.getHeight()));
		
		if (openButton.clicked()) {
			selectFile(win);
			if (filename != "") tipLabel.text = filename;
			Params::modified = fileChanged = true;
		}
		
		Params::update();
		
		if (Params::modified && okButton.clicked() || fileChanged) {
			Params::modified = fileChanged = false;
			Test::process(filename);
			updateGUI(leftArea);
		}
		
		if (Window::isKeyPressed(SDL_SCANCODE_ESCAPE)) break;
	}

//	Config::save();
	return 0;
}

