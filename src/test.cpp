#include "test.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <queue>
using std::vector;
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo.hpp>
#include "debug.h"
#include "logger.h"

#ifdef PROJECTNAME_TARGET_WINDOWS
#include <Windows.h>
inline double timer() {
	static LARGE_INTEGER counterFreq;
	if (counterFreq.QuadPart == 0) QueryPerformanceFrequency(&counterFreq);
	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	return (double)now.QuadPart / counterFreq.QuadPart;
}
#else
#include <chrono>
inline double timer() {
	using namespace std::chrono;
	high_resolution_clock::time_point now = high_resolution_clock::now();
	return duration_cast<duration<double> >(now.time_since_epoch()).count();
}
#endif

inline double clamp(double x, double mn, double mx) {
	return std::min(std::max(x, mn), mx);
}

inline int clampi(int x, int mn, int mx) {
	return std::min(std::max(x, mn), mx);
}

inline double rnd() {
	return rand() / (RAND_MAX + 1.0) + rand() / (RAND_MAX + 1.0) / (RAND_MAX + 1.0);
}

inline int random(int a, int b) {
	return int(std::floor(rnd() * (b - a))) + a;
}

std::pair<double, double> calcMean(const vector<double>& a) {
	double res = 0.0, std = 0.0;
	for (double x: a) res += x;
	res /= a.size();
	for (double x: a) std += (x - res) * (x - res);
	return std::make_pair(res, std / double(a.size() - 1));
}

namespace Test {
	int sort_num, aperture, radius, sampleCount, expansion, erode;
	double threshold, entropythres, a, b, fgFraction, dist, tt;
	Vec3d color;
	
	int numPasses = 0;
	TextureImage pass[MaxPasses], final;
	std::string passLabel[MaxPasses];
	
	TextureImage mat2tex(const cv::Mat& img) {
		TextureImage res(img.cols, img.rows, 3);
		for (int i = 0; i < img.rows; i++) for (int j = 0; j < img.cols; j++) {
			int r, g, b;
			if (img.type() == CV_8U) {
				r = g = b = img.at<unsigned char>(i, j);
			} else if (img.type() == CV_8UC3) {
				r = img.at<cv::Vec3b>(i, j)[2];
				g = img.at<cv::Vec3b>(i, j)[1];
				b = img.at<cv::Vec3b>(i, j)[0];
			} else {
				LogFatal("Test::mat2tex(): unsupported cv::Mat format!");
				Assert(false);
			}
			res.color(j, i, 0) = r;
			res.color(j, i, 1) = g;
			res.color(j, i, 2) = b;
		}
		return res;
	}

	cv::Mat tex2mat(const TextureImage& tex) {
		TextureImage tmp = tex;
		for (int i = 0; i < tmp.height(); i++) for (int j = 0; j < tmp.width(); j++) {
			int r, g, b;
			r = tmp.color(j, i, 2);
			g = tmp.color(j, i, 1);
			b = tmp.color(j, i, 0);
			tmp.color(j, i, 0) = r;
			tmp.color(j, i, 1) = g;
			tmp.color(j, i, 2) = b;
		}
		return cv::Mat(tex.height(), tex.width(), CV_8UC(tex.bytesPerPixel()), tmp.data(), tex.pitch()).clone();
	}
	
	void addPass(const std::string& label, const cv::Mat& img) {
		pass[numPasses] = mat2tex(img);
		passLabel[numPasses] = label;
		numPasses++;
//		cv::imwrite((label + ".png").c_str(), img);
		double tt1 = timer();
		std::stringstream ss;
		ss << label << ": " << (tt1 - tt) << "s";
		LogInfo(ss.str());
		tt = tt1;
	}
	
	vector<cv::Point> shape_from_contour(const cv::Mat& img, const vector<cv::Point>& contour) {
		cv::Mat mask(img.rows, img.cols, CV_8U);
		cv::drawContours(mask, vector<vector<cv::Point> >(1, contour), 0, cv::Scalar(1), cv::FILLED);
		vector<cv::Point> inside_points;
		for (int i = 0; i < mask.rows; i++) for (int j = 0; j < mask.cols; j++) {
			if (mask.at<unsigned char>(i, j) == 1) inside_points.push_back(cv::Point(i, j));
		}
		return inside_points;
	}

	double entropy_calculation(const cv::Mat& imgray, const vector<cv::Point>& coords) {
		int point_num = coords.size();
		double entropy_area = 0;
		vector<int> graycount(256, 0);
		for (int i = 0; i < point_num; i++) graycount[imgray.at<unsigned char>(coords[i].x, coords[i].y)]++;
		for (int i = 0; i < 256; i++) {
			double probability = graycount[i] / double(point_num);
			if (probability > 0) {
				double entropy = -probability * std::log(probability);
	            entropy_area += entropy;
			}
		}
//		std::cout << entropy_area << std::endl;
		return entropy_area;
	}
	
	void bfs(cv::Mat& d, int maxd) {
		const int rd[4] = {1, 0, -1, 0}, cd[4] = {0, 1, 0, -1};
		int n = d.rows, m = d.cols;
		std::queue<std::pair<int, int> > q;
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) if (d.at<int>(i, j) == 0) q.push(std::make_pair(i, j));
		while (!q.empty()) {
			std::pair<int, int> p = q.front();
			q.pop();
			if (d.at<int>(p.first, p.second) >= maxd) break;
			for (int k = 0; k < 4; k++) {
				int rr = p.first + rd[k], cc = p.second + cd[k];
				if (rr < 0 || cc < 0 || rr >= n || cc >= m) continue;
				if (d.at<int>(rr, cc) != -1) continue;
				d.at<int>(rr, cc) = d.at<int>(p.first, p.second) + 1;
				q.push(std::make_pair(rr, cc));
			}
		}
	}
	
	void bfs1(cv::Mat& d, cv::Mat& mask, int border) {
		const int rd[4] = {1, 0, -1, 0}, cd[4] = {0, 1, 0, -1};
		int n = d.rows, m = d.cols;
		std::queue<std::pair<int, int> > q;
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (mask.at<bool>(i, j) || i >= border && i < n - border && j >= border && j < m - border) continue;
			d.at<int>(i, j) = 0;
			q.push(std::make_pair(i, j));
		}
		while (!q.empty()) {
			std::pair<int, int> p = q.front();
			q.pop();
			for (int k = 0; k < 4; k++) {
				int rr = p.first + rd[k], cc = p.second + cd[k];
				if (rr < 0 || cc < 0 || rr >= n || cc >= m) continue;
				if (d.at<int>(rr, cc) != -1 || mask.at<bool>(rr, cc)) continue;
				d.at<int>(rr, cc) = d.at<int>(p.first, p.second) + 1;
				q.push(std::make_pair(rr, cc));
			}
		}
	}
	
	void process(const std::string& filename) {
		numPasses = 0;
		tt = timer();
		
		cv::Mat im = cv::imread(filename);
		if (im.data == nullptr) return;
	    
		int n = im.rows, m = im.cols;
		im = im(cv::Range(1, n - 1), cv::Range(1, m - 1));
		n = im.rows, m = im.cols;
		addPass("Original", im);
		
		cv::Mat denoised, edge;
		im.convertTo(denoised, CV_8UC3);
		std::vector<cv::Mat> channel;
		cv::split(denoised, channel);
		for (size_t i = 0; i < channel.size(); i++) cv::equalizeHist(channel[i], channel[i]);
		cv::merge(channel, denoised);
		
		cv::UMat denoisedu, edgeu;
		denoised.copyTo(denoisedu);
		cv::fastNlMeansDenoisingColored(denoisedu, denoisedu, 30, 100, 7, 21);
		cv::Canny(denoisedu, edgeu, a, a + b, 5, true);
//		denoisedu.copyTo(denoised);
		edgeu.copyTo(edge);
		
//		addPass("Enhanced", denoised);
		addPass("Canny", edge);
		
		cv::Mat d(n, m, CV_32S), res(n, m, CV_8UC3);

		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (edge.at<bool>(i, j)) d.at<int>(i, j) = 0; else d.at<int>(i, j) = -1;
		}
		bfs(d, expansion);
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (d.at<int>(i, j) == -1) d.at<int>(i, j) = 0; else d.at<int>(i, j) = -1;
		}
//		bfs(d, expansion);
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (d.at<int>(i, j) == -1) edge.at<unsigned char>(i, j) = 255;
			else edge.at<unsigned char>(i, j) = 0;
			d.at<int>(i, j) = -1;
		}
		addPass("Edges", edge);

		bfs1(d, edge, 3);
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (d.at<int>(i, j) != -1) res.at<cv::Vec3b>(i, j) = im.at<cv::Vec3b>(i, j);
			else res.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 0);
		}
		
		addPass("Residual", res);

/*
		cv::Mat imgray;
		cv::cvtColor(im, imgray, cv::COLOR_BGR2GRAY);
		addPass("gray", imgray);
		
		cv::Mat res(n, m, CV_8UC3), mean(n, m, CV_8U), std(n, m, CV_8U);
		
		TextureImage tex = mat2tex(imgray);
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			vector<double> neigh;
			for (int k = 0; k < sampleCount; k++) {
				int y = clampi(random(i - radius, i + radius + 1), 0, n - 1);
				int x = clampi(random(j - radius, j + radius + 1), 0, m - 1);
				neigh.push_back(tex.color(x, y, 0));
			}
			std::pair<double, double> meanstd = calcMean(neigh);
			mean.at<unsigned char>(i, j) = clamp(meanstd.first, 0, 255);
			std.at<unsigned char>(i, j) = clamp(meanstd.second, 0, 255);
			double curr = meanstd.second;
			res.at<cv::Vec3b>(i, j)[0] = res.at<cv::Vec3b>(i, j)[1] = res.at<cv::Vec3b>(i, j)[2] = 0;
			if (curr < threshold) res.at<cv::Vec3b>(i, j) = im.at<cv::Vec3b>(i, j);
		}
		
//		addPass("scaled", im);
//		addPass("mean", mean);
		addPass("std", std);
		addPass("res", res);

*/

		n = res.rows, m = res.cols;
		cv::Mat up, down, left, right, test = res.clone();
		res.convertTo(up, CV_8UC3);
		res.convertTo(down, CV_8UC3);
		res.convertTo(left, CV_8UC3);
		res.convertTo(right, CV_8UC3);
		cv::Mat upd(n, m, CV_32S), downd(n, m, CV_32S), leftd(n, m, CV_32S), rightd(n, m, CV_32S);
		
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			upd.at<int>(i, j) = downd.at<int>(i, j) = leftd.at<int>(i, j) = rightd.at<int>(i, j) = 0;
		}
		
		for (int i = 1; i < n; i++) for (int j = 0; j < m; j++) {
			if (res.at<cv::Vec3b>(i, j)[0] == 0) {
				up.at<cv::Vec3b>(i, j) += up.at<cv::Vec3b>(i - 1, j);
				upd.at<int>(i, j) = upd.at<int>(i - 1, j) + 1;
			}
		}
		for (int i = n - 2; i >= 0; i--) for (int j = 0; j < m; j++) {
			if (res.at<cv::Vec3b>(i, j)[0] == 0) {
				down.at<cv::Vec3b>(i, j) += down.at<cv::Vec3b>(i + 1, j);
				downd.at<int>(i, j) = downd.at<int>(i + 1, j) + 1;
			}
		}
		for (int i = 0; i < n; i++) for (int j = 1; j < m; j++) {
			if (res.at<cv::Vec3b>(i, j)[0] == 0) {
				left.at<cv::Vec3b>(i, j) += left.at<cv::Vec3b>(i, j - 1);
				leftd.at<int>(i, j) = leftd.at<int>(i, j - 1) + 1;
			}
		}
		for (int i = 0; i < n; i++) for (int j = m - 2; j >= 0; j--) {
			if (res.at<cv::Vec3b>(i, j)[0] == 0) {
				right.at<cv::Vec3b>(i, j) += right.at<cv::Vec3b>(i, j + 1);
				rightd.at<int>(i, j) = rightd.at<int>(i, j + 1) + 1;
			}
		}
		
		for (int i = 1; i < n; i++) for (int j = 0; j < m; j++) {
			if (res.at<cv::Vec3b>(i, j)[0] == 0) {
				double count = 0;
				cv::Vec3d curr = 0;
				if (up.at<cv::Vec3b>(i, j) != cv::Vec3b(0, 0, 0)) {
					double k = 1.0 / double(upd.at<int>(i, j));
					curr += cv::Vec3d(up.at<cv::Vec3b>(i, j)) * k;
					count += k;
				}
				if (down.at<cv::Vec3b>(i, j) != cv::Vec3b(0, 0, 0)) {
					double k = 1.0 / double(downd.at<int>(i, j));
					curr += cv::Vec3d(down.at<cv::Vec3b>(i, j)) * k;
					count += k;
				}
				if (left.at<cv::Vec3b>(i, j) != cv::Vec3b(0, 0, 0)) {
					double k = 1.0 / double(leftd.at<int>(i, j));
					curr += cv::Vec3d(left.at<cv::Vec3b>(i, j)) * k;
					count += k;
				}
				if (right.at<cv::Vec3b>(i, j) != cv::Vec3b(0, 0, 0)) {
					double k = 1.0 / double(rightd.at<int>(i, j));
					curr += cv::Vec3d(right.at<cv::Vec3b>(i, j)) * k;
					count += k;
				}
				test.at<cv::Vec3b>(i, j) = curr / count;
			}
		}
		
		addPass("Interpolated", test);
		
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			cv::Vec3b a = test.at<cv::Vec3b>(i, j), b = im.at<cv::Vec3b>(i, j);
			test.at<cv::Vec3b>(i, j) = (a - b) * 4 + cv::Vec3b(20, 20, 20);
		}
		
		addPass("Difference", test);
		
		cv::UMat testu;
		test.copyTo(testu);
		cv::fastNlMeansDenoisingColored(testu, testu, 10, 10, 7, 21);
		testu.copyTo(test);
		addPass("Denoised", test);
		
		cv::Mat matched;
		im.convertTo(matched, CV_8UC3);
		cv::Mat f(n, m, CV_32S);
		
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			cv::Vec3b colcv = test.at<cv::Vec3b>(i, j);
			Vec3d col(colcv[0], colcv[1], colcv[2]);
			if ((col - color).length() <= dist) {
				matched.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 0, 255);
				f.at<int>(i, j) = -1;
			} else f.at<int>(i, j) = 0;
		}
		
		addPass("Matches", matched);
		
		im.convertTo(matched, CV_8UC3);
/*
		bfs(f, erode);
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (f.at<int>(i, j) == -1) f.at<int>(i, j) = 0;
			else f.at<int>(i, j) = -1;
		}
		bfs(f, erode);
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (f.at<int>(i, j) == -1) f.at<int>(i, j) = -1;
			else f.at<int>(i, j) = 0;
		}
*/
		bfs(f, erode);
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (f.at<int>(i, j) == -1) f.at<int>(i, j) = 0;
			else f.at<int>(i, j) = -1;
		}
		bfs(f, erode);
		for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
			if (f.at<int>(i, j) != -1) matched.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 0, 255);
		}
		
		addPass("Final", matched);
		
/*
		cv::Mat imgray;
		cv::cvtColor(im, imgray, cv::COLOR_BGR2GRAY);
		addPass("gray", imgray);

		// ֱ\B7\BDͼ\BE\F9\BA\E2
		cv::equalizeHist(imgray, imgray);
		addPass("enhanced", imgray);

		// ȥ\B1߿\F2
		int m = imgray.rows, n = imgray.cols;
		imgray = imgray(cv::Range(1, m - 1), cv::Range(1, n - 1));

		// (\B1\DFԵ\BC\EC\B2\E2)
		cv::Mat im_canny;
		cv::Canny(imgray, im_canny, a, b, aperture - aperture % 2 + 1);
		addPass("canny", im_canny);

		// \B6\FEֵ\BB\AF
		cv::Mat thresh;
		cv::threshold(imgray, thresh, threshold, 255, cv::THRESH_BINARY_INV);
		addPass("thresh", thresh);

		// \D5\D2\C2\D6\C0\AA
		vector<vector<cv::Point> > contours;
		vector<cv::Vec4i> hierarchy;
		cv::findContours(thresh, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);

//		cv::drawContours(im, contours, -1, cv::Scalar(0, 0, 255), 3);
//		addPass("contour", im);

		// \BC\C6\CB\E3\C7\F8\D3\F2\B4\F3С
		vector<double> area(contours.size(), 0);
		for (size_t i = 0; i < contours.size(); i++) {
			vector<cv::Point> cnt = contours[i];
			area[i] = cv::contourArea(cnt);
		}

		// \B8\F9\BE\DD\C7\F8\D3\F2\B4\F3С\C5\C5\D0\F2
		vector<int> area_index(area.size());
		for (size_t i = 0; i < area_index.size(); i++) area_index[i] = i;
		std::sort(area_index.begin(), area_index.end(), [&](int x, int y) { return area[x] > area[y]; });
		
		if (sort_num > contours.size()) sort_num = contours.size();
		vector<vector<cv::Point> > contours_output(sort_num);
		for (int i = 0; i < sort_num; i++) contours_output[i] = contours[area_index[i]];

		// \BC\C6\CB\E3\EC\D8
		vector<double> entropy(sort_num, 0);
		for (int i = 0; i < sort_num; i++) {
			vector<cv::Point> coords = shape_from_contour(im, contours_output[i]);
	    	entropy[i] = entropy_calculation(thresh, coords);
		}

		// ͼ\CF\F1\ECؼ\C6\CB㡢\C5ж\CF
		vector<double> entropy_output = entropy;
		vector<int> entropy_index(entropy.size());
		for (size_t i = 0; i < entropy_index.size(); i++) entropy_index[i] = i;
		std::sort(entropy_index.begin(), entropy_index.end(), [&](int x, int y) { return entropy[x] < entropy[y]; });
		
		vector<vector<cv::Point> > contours_final(sort_num);
		int tgtnum = 0;
		for (int i = 0; i < sort_num; i++) {
			if (entropy_output[i] < entropythres) {
				contours_final[tgtnum] = contours_output[i];
				tgtnum++;
			}
		}
		contours_final.resize(tgtnum);

		// \CA\E4\B3\F6\BD\E1\B9\FB
		cv::drawContours(im, contours_final, -1, cv::Scalar(255, 0, 255), 3);
		addPass("output", im);
*/
		
		if (numPasses > 0) final = pass[numPasses - 1];
	}
}

