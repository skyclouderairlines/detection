#include <string>
#include "texture.h"
#include "vec.h"

namespace Test {
	extern int sort_num, aperture, radius, sampleCount, expansion, erode;
	extern double threshold, entropythres, a, b, fgFraction, dist;
	extern Vec3d color;
	
	const int MaxPasses = 233;
	extern int numPasses;
	extern TextureImage pass[MaxPasses], final;
	extern std::string passLabel[MaxPasses];
	
	void process(const std::string& filename);
}

